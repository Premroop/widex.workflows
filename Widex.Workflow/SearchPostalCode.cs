﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Query;

namespace Widex.Workflow
{
    public sealed class SearchPostalCode : CodeActivity
    {
        [RequiredArgument]
        [Input("Enter the postal code to search")]
        public InArgument<string> PostalCode { get; set; }

        [Output("Sales Rep")]
        [ReferenceTarget("systemuser")]
        public OutArgument<EntityReference> SaleRepUser { get; set; }

        [Output("Outcome")]
        [Default("")]//empty string
        public OutArgument<string> Outcome { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            // Create the tracing service
            ITracingService tracingService = executionContext.GetExtension<ITracingService>();

            if (tracingService == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve tracing service.");
            }

            tracingService.Trace("Entered Execute(), Activity Instance Id: {0}, Workflow Instance Id: {1}",
                executionContext.ActivityInstanceId,
                executionContext.WorkflowInstanceId);

            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

            tracingService.Trace("Workflow Execute(), Correlation Id: {0}, Initiating User: {1}",
               context.CorrelationId,
               context.InitiatingUserId);

            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            try
            {
                tracingService.Trace("Entity Id:{0}", context.PrimaryEntityId);

                if (PostalCode != null) //null check is not required as it is a required argument.
                {
                    string postalCode = PostalCode.Get(executionContext).Trim();

                    //Search for the postal code record.                  
                    // Instantiate QueryExpression QEwid_postalcode
                    var QEwid_postalcode = new QueryExpression("wid_postalcode");
                    QEwid_postalcode.TopCount = 10;

                    // Add columns to QEwid_postalcode.ColumnSet
                    QEwid_postalcode.ColumnSet.AddColumns("wid_salesrep");//, "wid_insidesalesrep");

                    // Define filter QEwid_postalcode.Criteria
                    QEwid_postalcode.Criteria.AddCondition("wid_name", ConditionOperator.Equal, postalCode);

                    // Retrieve Postal Code data
                    var pcData = service.RetrieveMultiple(QEwid_postalcode);

                    // Check if there is any postal code record.
                    if (pcData != null && pcData.Entities.Count > 0)
                    {
                        //If result is one then return the sales rep as output param.
                        if (pcData.Entities.Count == 1)
                        {
                            if (pcData.Entities[0].Contains("wid_salesrep"))
                            {
                                EntityReference salesRep = (EntityReference)pcData.Entities[0]["wid_salesrep"];
                                SaleRepUser.Set(executionContext, salesRep);
                                Outcome.Set(executionContext, "One match - has sales rep");
                            }
                            else
                            {
                                Outcome.Set(executionContext, "One match - does not have sales rep");
                            }
                        }
                        else
                        {
                            //found multiple matches. Return error.
                            Outcome.Set(executionContext, "Multiple matches");
                        }
                    }
                    else
                    {
                        //no record found. Set blank value
                        SaleRepUser.Set(executionContext, null);
                        Outcome.Set(executionContext, "No matches");
                    }

                }

                tracingService.Trace("Workflow execution completed.");
            }

            catch (FaultException<OrganizationServiceFault> ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace("Timestamp: {0}", ex.Detail.Timestamp);
                tracingService.Trace("Code: {0}", ex.Detail.ErrorCode);
                tracingService.Trace("Message: {0}", ex.Detail.Message);
                tracingService.Trace("Inner Fault: {0}",
                    null == ex.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
            }
            catch (System.TimeoutException ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace("Message: {0}", ex.Message);
                tracingService.Trace("Stack Trace: {0}", ex.StackTrace);
                tracingService.Trace("Inner Fault: {0}",
                    null == ex.InnerException.Message ? "No Inner Fault" : ex.InnerException.Message);
            }
            catch (System.Exception ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace(ex.Message);

                // Display the details of the inner exception.
                if (ex.InnerException != null)
                {
                    tracingService.Trace(ex.InnerException.Message);

                    FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> fe = ex.InnerException
                        as FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>;
                    if (fe != null)
                    {
                        tracingService.Trace("Timestamp: {0}", fe.Detail.Timestamp);
                        tracingService.Trace("Code: {0}", fe.Detail.ErrorCode);
                        tracingService.Trace("Message: {0}", fe.Detail.Message);
                        tracingService.Trace("Trace: {0}", fe.Detail.TraceText);
                        tracingService.Trace("Inner Fault: {0}",
                            null == fe.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
                    }
                }
            }
        }
    }
}
