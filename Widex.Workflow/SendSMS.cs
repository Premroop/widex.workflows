﻿using System;
using System.Activities;
using System.Linq;
using System.ServiceModel;

using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Widex.Workflow
{
    public sealed class SendSMS : CodeActivity
    {
        [RequiredArgument]
        [Input("Recipient Phone Number")]
        public InArgument<string> SendMessageTo { get; set; }

        [RequiredArgument]
        [Input("Sender Phone Number or Code Ex: TM-Widex")]
        public InArgument<string> SendMessageFrom { get; set; }

        [RequiredArgument]
        [Input("Text Message")]
        public InArgument<string> Body { get; set; }

        [Output("Message Id")]
        public OutArgument<string> MessageId { get; set; }

        protected override void Execute(CodeActivityContext executionContext)
        {
            // Create the tracing service
            ITracingService tracingService = executionContext.GetExtension<ITracingService>();

            if (tracingService == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve tracing service.");
            }

            tracingService.Trace("Entered Execute(), Activity Instance Id: {0}, Workflow Instance Id: {1}",
                executionContext.ActivityInstanceId,
                executionContext.WorkflowInstanceId);

            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

            tracingService.Trace("Workflow Execute(), Correlation Id: {0}, Initiating User: {1}",
               context.CorrelationId,
               context.InitiatingUserId);

            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            try
            {
                tracingService.Trace("Entity Id:{0}", context.PrimaryEntityId);

                OrganizationServiceContext svcContext = new OrganizationServiceContext(service);

                var apiKeyData = (from lc in svcContext.CreateQuery("new_localconfiguration")
                                  where ((string)lc["new_name"]).Contains("Twilio")
                                  select new { ApiKey = lc["new_name"], ApiValue = lc["new_value"] }).ToList();

                var accountSid = string.Empty;
                var authToken = string.Empty;

                foreach (var apiKey in apiKeyData)
                {
                    if (apiKey.ApiKey.ToString().ToLower().Trim() == "twilioaccountsid")
                    {
                        // Your Account SID from twilio.com/console
                        accountSid = apiKey.ApiValue.ToString().Trim();
                    }
                    else
                    {
                        if (apiKey.ApiKey.ToString().ToLower().Trim() == "twilioauthtoken")
                        {
                            // Your Auth Token from twilio.com/console
                            authToken = apiKey.ApiValue.ToString().Trim();
                        }
                    }
                }

                

                var toPhone = SendMessageTo.Get(executionContext);

                TwilioClient.Init(accountSid, authToken);

                var message = MessageResource.Create(
                    to: new PhoneNumber(toPhone),
                    from: new PhoneNumber(SendMessageFrom.Get(executionContext)),
                    body: Body.Get(executionContext));

                // statusCallback: new Uri("https://prod-242.westeurope.logic.azure.com:443/workflows/ff971af4fcb345a3b769ae33c503d18c/triggers/manual/paths/invoke?api-version=2016-06-01&sp=%2Ftriggers%2Fmanual%2Frun&sv=1.0&sig=S1cnLnpR1--DaTxV-ErrnybSJ1cL-kFInlPVbPG0cIQ"),
                //Console.WriteLine(message.Sid);

                MessageId.Set(executionContext, message.Sid);

                tracingService.Trace("Message Sent successfully {0}", message.Sid);

                //throw new InvalidWorkflowException("Custom Error thrown:" + message.Sid);

               tracingService.Trace("Workflow execution completed.");
                
            }
            catch (FaultException<OrganizationServiceFault> ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace("Timestamp: {0}", ex.Detail.Timestamp);
                tracingService.Trace("Code: {0}", ex.Detail.ErrorCode);
                tracingService.Trace("Message: {0}", ex.Detail.Message);
                tracingService.Trace("Inner Fault: {0}",
                    null == ex.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
            }
            catch (System.TimeoutException ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace("Message: {0}", ex.Message);
                tracingService.Trace("Stack Trace: {0}", ex.StackTrace);
                tracingService.Trace("Inner Fault: {0}",
                    null == ex.InnerException.Message ? "No Inner Fault" : ex.InnerException.Message);
            }
            catch (System.Exception ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace(ex.Message);

                // Display the details of the inner exception.
                if (ex.InnerException != null)
                {
                    tracingService.Trace(ex.InnerException.Message);

                    FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> fe = ex.InnerException
                        as FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>;
                    if (fe != null)
                    {
                        tracingService.Trace("Timestamp: {0}", fe.Detail.Timestamp);
                        tracingService.Trace("Code: {0}", fe.Detail.ErrorCode);
                        tracingService.Trace("Message: {0}", fe.Detail.Message);
                        tracingService.Trace("Trace: {0}", fe.Detail.TraceText);
                        tracingService.Trace("Inner Fault: {0}",
                            null == fe.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
                    }
                }
            }
        }
    }
}
