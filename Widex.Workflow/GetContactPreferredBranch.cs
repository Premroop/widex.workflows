﻿using System.Activities;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;
using System.ServiceModel;
using Microsoft.Xrm.Sdk.Query;
using System;

namespace Widex.Workflow
{
    public sealed class GetContactPreferredBranch : CodeActivity
    {
        [Output("Preferred Branch")]
        [ReferenceTarget("account")]
        public OutArgument<EntityReference> PreferredBranch { get; set; }

        [Output("External Link")]
        [Default("")]//empty string
        public OutArgument<string> ExternalLink { get; set; }


        protected override void Execute(CodeActivityContext executionContext)
        {
            // Create the tracing service
            ITracingService tracingService = executionContext.GetExtension<ITracingService>();

            if (tracingService == null)
            {
                throw new InvalidPluginExecutionException("Failed to retrieve tracing service.");
            }

            tracingService.Trace("Entered Execute(), Activity Instance Id: {0}, Workflow Instance Id: {1}",
                executionContext.ActivityInstanceId,
                executionContext.WorkflowInstanceId);

            IWorkflowContext context = executionContext.GetExtension<IWorkflowContext>();

            tracingService.Trace("Workflow Execute(), Correlation Id: {0}, Initiating User: {1}",
               context.CorrelationId,
               context.InitiatingUserId);

            IOrganizationServiceFactory serviceFactory = executionContext.GetExtension<IOrganizationServiceFactory>();
            IOrganizationService service = serviceFactory.CreateOrganizationService(context.UserId);

            try
            {
                tracingService.Trace("Entity Id:{0}", context.PrimaryEntityId);

                Entity phoneCall = service.Retrieve("phonecall", context.PrimaryEntityId, new ColumnSet("to"));

                EntityCollection to = phoneCall.GetAttributeValue<EntityCollection>("to");
                if (to != null)
                {
                    //Only going to check for the first value. Check if it is contact
                    if (((EntityReference)to.Entities[0].Attributes["partyid"]).LogicalName == "contact")
                    {
                        //Get contactid
                        Guid contactId = ((EntityReference)to.Entities[0].Attributes["partyid"]).Id;

                        //Retrieve the contact preferred branch details.
                        Entity contact = service.Retrieve("contact", contactId, new ColumnSet("new_preferredpointofsales", "new_externallink"));
                        if (contact.Contains("new_preferredpointofsales"))
                        {
                            PreferredBranch.Set(executionContext, contact["new_preferredpointofsales"]);
                        }
                        else
                        {
                            PreferredBranch.Set(executionContext, null);
                        }
                        if (contact.Contains("new_externallink"))
                        {
                            ExternalLink.Set(executionContext, contact["new_externallink"]);
                        }
                    }
                }

                tracingService.Trace("Workflow execution completed.");
            }

            catch (FaultException<OrganizationServiceFault> ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace("Timestamp: {0}", ex.Detail.Timestamp);
                tracingService.Trace("Code: {0}", ex.Detail.ErrorCode);
                tracingService.Trace("Message: {0}", ex.Detail.Message);
                tracingService.Trace("Inner Fault: {0}",
                    null == ex.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
            }
            catch (System.TimeoutException ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace("Message: {0}", ex.Message);
                tracingService.Trace("Stack Trace: {0}", ex.StackTrace);
                tracingService.Trace("Inner Fault: {0}",
                    null == ex.InnerException.Message ? "No Inner Fault" : ex.InnerException.Message);
            }
            catch (System.Exception ex)
            {
                tracingService.Trace("The application terminated with an error.");
                tracingService.Trace(ex.Message);

                // Display the details of the inner exception.
                if (ex.InnerException != null)
                {
                    tracingService.Trace(ex.InnerException.Message);

                    FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault> fe = ex.InnerException
                        as FaultException<Microsoft.Xrm.Sdk.OrganizationServiceFault>;
                    if (fe != null)
                    {
                        tracingService.Trace("Timestamp: {0}", fe.Detail.Timestamp);
                        tracingService.Trace("Code: {0}", fe.Detail.ErrorCode);
                        tracingService.Trace("Message: {0}", fe.Detail.Message);
                        tracingService.Trace("Trace: {0}", fe.Detail.TraceText);
                        tracingService.Trace("Inner Fault: {0}",
                            null == fe.Detail.InnerFault ? "No Inner Fault" : "Has Inner Fault");
                    }
                }
            }
        }
    }
}
